/**  Terdapat sebuah array of strings sebagai berikut:

['cook', 'save', 'taste', 'aves', 'vase', 'state', 'map']

Buatlah sebuah fungsi yang mengelompokkan sebuah array of strings di atas menjadi kumpulan anagram, dengan expected result sebagai berikut:

[
  [ 'cook' ],
  [ 'save', 'aves', 'vase' ],
  [ 'taste', 'state' ],
  [ 'map' ]
]

Catatan: dilarang menggunakan built in function seperti array.map, array.reduce, dan array.filter.
*/

// 1. Urutkan abjad pada setiap kata
// 2. Kelompokkan

function urutkanAbjad(str) {
    let result = "";
    const abjad = 'abcdefghijklmnopqrstuvwxyz';
    for (let i = 0; i < abjad.length; i++) {
        for (let j = 0; j < str.length; j++) {
            if (str[j] === abjad[i]) {
                result += str[j];
            }
            // console.log(abjad[i])
        }
    }
    return result;
}

// console.log(urutkanAbjad('map'));


function groupAnagrams(str) {
    let kelompokAnagram = {};

    for (let i = 0; i < str.length; i++) {
        let anagram = urutkanAbjad(str[i]);
        if (kelompokAnagram[anagram]) {
            kelompokAnagram[anagram].push(str[i]);
        } else {
            kelompokAnagram[anagram] = [str[i]];
        }
    }
    return Object.values(kelompokAnagram);
}

console.log(groupAnagrams(['cook', 'save', 'taste', 'aves', 'vase', 'state', 'map']));
